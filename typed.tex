\section{Functional Name Component: Typed Components}

In a variety of situations, name components need to represent commands or annotations that can appear at arbitrary levels in the name hierarchy, intermixed with the human readable components that are analogous to pieces of file names.
In these cases, the position in the name is not able to distinguish the component's role and hence one must rely on coding conventions in the component itself.
Wherever possible, however, the function or meaning of a name component should be determined by its context within the overall name and under the specific application semantics, and human readable components should be used where they will not be mistaken for functional name components.

The NDN Packet Format specification, since version 0.3~\cite{ndn-tlv}, allows 65535 different TLV-TYPEs in name components.
Several TLV-TYPEs have been reserved for functional name components, in accordance with the Name Component Assignment policy~\cite{NameComponentType}.
The following sections define the recommended semantics of these specific functions: segmenting, versioning, time stamping, and sequencing.
Note that the meaning of these name component types is application-layer information.
From the router's perspective these functional components are no different from other name components.
Although the specific semantics of the special name components is ultimately defined by the application, the consistent use of naming conventions helps applications get the most advantage from the NDN architecture.
For example, proper use of segmenting convention can be beneficial to applications, if NDN routers prioritize caching of ``complete'' collections of Data packets.


\subsection{Sequencing}

Sequential data collections are a common feature in many NDN applications.
The NDN Forwarding Daemon~(NFD)~\cite{nfd} uses such collections as part of the face status notification protocol; the ChronoSync~\cite{ChronoSync} protocol provides efficient synchronization primitives for sequenced data collections.
Each item in these collections is numbered using monotonically increasing non-negative integers starting from zero.
Collection items should be complete application-specific data items, and may or may not depend on other items of the same collection.

The following property, that can be leveraged by third-party applications, should hold for the items in a sequential data collection.
If it is known that there exists a collection item with sequence number $n$, then it is implied that items $0, 1, \ldots, (n-1)$ have already been produced and item $(n+1)$ may exist or may be produced in the future.
Note that this property does not require or guarantee that all ``previous'' items can be retrieved.

\vspace{.1in}
\begin{Verbatim}
    SequenceNumNameComponent = SEQUENCE-NUM-NAME-COMPONENT-TYPE TLV-LENGTH
                                 nonNegativeInteger

    SEQUENCE-NUM-NAME-COMPONENT-TYPE = %x3a
\end{Verbatim}

A sequence number should be encoded as \texttt{SequenceNumNameComponent}, with the sequence number (monotonically increasing non-negative integer) of the data item in the collection as its TLV-VALUE.
Each data item is either an individual Data packet or a collection of multiple segmented Data packets.


\subsection{Segmenting}

Segmenting is used when a large piece of data is cut into several chunks (or segments).
Each individual segment is assigned its own unique name and can be individually retrieved.
Although not defined in this document, applications should strive to define segmentation in a way that makes it possible to process individual segments on their own, without requiring to wait until the whole data is assembled.
For example, a large video frame could be split into segments, each individually contributing complete pieces of the frame encoding.

\vspace{.1in}
\begin{Verbatim}
    SegmentNameComponent = SEGMENT-NAME-COMPONENT-TYPE TLV-LENGTH
                             nonNegativeInteger

    ByteOffsetNameComponent = BYTE-OFFSET-NAME-COMPONENT-TYPE TLV-LENGTH
                                nonNegativeInteger

    SEGMENT-NAME-COMPONENT-TYPE = %x32
    BYTE-OFFSET-NAME-COMPONENT-TYPE = %x34
\end{Verbatim}

For segmenting purposes, we define \texttt{SegmentNameComponent} for sequence-based segmentation and\linebreak \texttt{ByteOffsetNameComponent} for byte-offset segmentation.
When sequence-based segmentation is used, each segment should be assigned a monotonically-increasing zero-based integer number: 0, 1, 2, \ldots.
The segment number is encoded in the TLV-VALUE of \texttt{SegmentNameComponent}.
When byte-offset segmentation is used, each segment should be assigned a byte offset (number of bytes) from the beginning of the segmented content: 0, (content size in first segment), (sum of content sizes in first two segments), \ldots.
The segment number is encoded in the TLV-VALUE of \texttt{ByteOffsetNameComponent}.

It is recommended to put \texttt{SegmentNameComponent} and \texttt{ByteOffsetNameComponent} as the last explicit name component.


\subsection{Versioning}

In NDN, all Data packets are immutable and cannot be changed after being published.
Therefore, when an applications needs to publish a ``new'' version of the content, it should be published under a new unique name using a proper versioning protocol.
The main goal and property of the versioning protocol is to explicitly define the name-based ordering of Data versions: the ``larger'' the name is (in the canonical ordering~\cite{ndn-tlv}), the more recent the Data is.%
\footnote{Without using a versioning protocol, the implicit digest name component still makes all Data packets unique, but it is impossible to determine the most recent version based on the naming alone.}
This property can be used by third-parties, e.g., to prioritize caching of the latest version of the data.

\vspace{.1in}
\begin{Verbatim}
    VersionNameComponent = VERSION-NAME-COMPONENT-TYPE TLV-LENGTH
                             nonNegativeInteger

    VERSION-NAME-COMPONENT-TYPE = %x36
\end{Verbatim}

\texttt{VersionNameComponent} contains a version number.
The specific value is determined by the applications and it is responsibility of the application to ensure that the larger value is assigned to the later version of the Data.

Timestamps are commonly used as the version value.
If using timestamps, it is recommended to use the number of microseconds since Unix epoch.
In a multi-producer application, if the clocks are synchronized, it is trivial to assign non-decreasing version numbers without introducing a version synchronization protocol.
If accurate clock synchronization is not feasible, the application should use non-time-based versioning and/or make all attempts to ensure proper versioning in some other way.
Otherwise, the incorrect use of \texttt{VersionNameComponent} may be harmful to the application, as third-parties may incorrectly treat the Data packet with the ``largest'' version as the latest (e.g., incorrectly prioritize caching of older versions).

It is recommended to put \texttt{VersionNameComponent} just before \texttt{SegmentNameComponent} or\linebreak \texttt{ByteOffsetNameComponent} in segmented content, or as the last explicit name component in non-segmented content.


\subsection{Time Stamping}

Some NDN applications may need to explicitly define when a specific Data packet was produced.
Even without synchronized clocks, this value can be used by third-parties to infer the gap between the creation times of different Data packets by the same producer, and this knowledge can be used in some way (e.g., caches that try to keep time-diverse Data packets/measurements).

\vspace{.1in}
\begin{Verbatim}
    TimestampNameComponent = TIMESTAMP-NAME-COMPONENT-TYPE TLV-LENGTH
                               nonNegativeInteger

    TIMESTAMP-NAME-COMPONENT-TYPE = %x38
\end{Verbatim}

\texttt{TimestampNameComponent} should be used when adding a timestamp to the name.
The TLV-VALUE should be the number of microseconds since Unix epoch, not counting leap seconds, generated from the system clock to indicate the creation time of the Data.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "convention"
%%% End:
